<?php

namespace BoostN\SlackNotify;

use XF\Mvc\Entity\Entity;

class Listener
{
    public static function errorEntityStructure(\XF\Mvc\Entity\Manager $em, \XF\Mvc\Entity\Structure &$structure)
    {
        $structure->columns['boostn_slack_notify'] = ['type' => Entity::BOOL, 'default' => false];
    }
}