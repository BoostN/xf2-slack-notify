<?php

namespace BoostN\SlackNotify\Job;

use XF\Job\AbstractJob;

class ErrorLog extends AbstractJob
{
    public function run($maxRunTime)
    {
        $app = \XF::app();

        if ($app->options()->boostnSlackNotifyenableErrorLogReport) {
            /**
             * @var \BoostN\SlackNotify\Repository\ErrorLog $errorRepo
             */
            $errorRepo = \XF::repository('BoostN\SlackNotify:ErrorLog');
            $latestErrors = $errorRepo->getErrors();

            if ($latestErrors >= 1) {
                //CALL SLACK API
                /** @var \BoostN\SlackNotify\Service\SlackNotify $slackService */
                $slackService = \XF::service('BoostN\SlackNotify:SlackNotify');

                //build url to post to slack
                $boardUrl = $app->options()->boardUrl;
                $errorLogUrl = '/admin.php?logs/server-errors/';
                $phrase = \XF::phrase('boostn_slacknotify_server_error');

                //Build payload to send to Slack
                $payload = array(
                    'attachments' =>
                        array(),
                    'channel' => $app->options()->boostnSlackNotifychannelName,
                    'icon_emoji' => $app->options()->boostnSlackNotifyenableErrorLogReportIcon,
                    'icon_url' => NULL,
                    'text' => $latestErrors . " ". $phrase . "\n" . $boardUrl . $errorLogUrl,
                    'username' => $app->options()->boostnSlackNotifyuserName,
                );

                $slackService->postToSlack($payload);

                //set error as sent!
                $errorRepo->setError($latestErrors);
            }

            return $this->complete();
        }

    }



    public function getStatusMessage()
    {
        $actionPhrase = \XF::phrase('rebuilding');
        $typePhrase = 'Gathering Error Logs';
    }

    public function canCancel()
    {
        return true;
    }

    public function canTriggerByChoice()
    {
        return true;
    }
}