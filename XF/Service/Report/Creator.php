<?php

namespace BoostN\SlackNotify\XF\Service\Report;

use XF\Entity\User;

class Creator extends XFCP_Creator
{
    protected $user;

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function sendNotifications()
    {
        parent::sendNotifications();

        $app = \XF::app();

        $channel = $app->options()->boostnSlackNotifychannelName;
        $icon = $app->options()->boostnSlackNotifyenableErrorLogReportIcon;
        $reportPhrase = \XF::phrase('boostn_slacknotify_user_report_created');

        if($app->options()->boostnSlackNotifyenableNewThreadreport)
        {
            /** @var \BoostN\SlackNotify\Service\SlackNotify $slackService */
            $slackService = \XF::service('BoostN\SlackNotify:SlackNotify');

            //Build payload to send to Slack
            $payload = array (
                'attachments' =>
                    array (
                    ),
                'channel' => $channel,
                'icon_emoji' => $icon,
                'icon_url' => NULL,
                'text' => $reportPhrase,
                'username' => $app->options()->boostnSlackNotifyuserName,
            );

            $slackService->postToSlack($payload);
        }

    }
}