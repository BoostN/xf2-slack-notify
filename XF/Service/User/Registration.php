<?php

namespace BoostN\SlackNotify\XF\Service\User;


class Registration extends XFCP_Registration
{
    protected function sendRegistrationContact()
    {
        parent::sendRegistrationContact();

        /** @var \BoostN\SlackNotify\Service\SlackNotify $slackService */
        $slackService = \XF::service('BoostN\SlackNotify:SlackNotify');

        $app = \XF::app();
        $user = $this->user;


        $channel = $app->options()->boostnSlackNotifychannelName;
        $icon = $app->options()->boostnSlackNotifygeneral_icon ?: '';
        $modPhrase = \XF::phrase('boostn_slacknotify_user_needs_approval');
        $regPhrase = \XF::phrase('boostn_slacknotify_user_reg_account');

        //Build payload to send to Slack
        $payload = array (
            'attachments' =>
                array (
                ),
            'channel' => $channel,
            'icon_emoji' => $icon,
            'icon_url' => NULL,
            'text' => $user->username . $regPhrase,
            'username' => $app->options()->boostnSlackNotifyuserName,
        );

        if($app->options()->boostnSlackNotifyenableNewUserReg)
        {
            $slackService->postToSlack($payload);
        }

        if($app->options()->boostnSlackNotifyenableNewUserModerated && $user->user_state == 'moderated')
        {
            $nPayload = array (
                'text' => $user->username . $modPhrase
            );
            $payload = array_merge($payload,$nPayload);

            $slackService->postToSlack($payload);
        }
    }
}