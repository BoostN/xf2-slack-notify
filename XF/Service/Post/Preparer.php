<?php

namespace BoostN\SlackNotify\XF\Service\Post;

use XF\Entity\Post;

class Preparer extends XFCP_Preparer
{
    public function getPost()
    {
        return $this->post;
    }

    public function afterInsert()
    {
        parent::afterInsert();

        $post = $this->post;
        $thread = $this->post->Thread;

        $app = \XF::app();

        $channel = $app->options()->boostnSlackNotifychannelName;
        $icon = $app->options()->boostnSlackNotifygeneral_icon ?: '';
        $reportPhrasePost = \XF::phrase('boostn_slacknotify_post_moderated');
        $reportPhraseThread = \XF::phrase('boostn_slacknotify_thread_moderated');

        /** @var \BoostN\SlackNotify\Service\SlackNotify $slackService */
        $slackService = \XF::service('BoostN\SlackNotify:SlackNotify');

        if($app->options()->boostnSlackNotifyenableNewModeratedPost)
        {

            if ($post->isFirstPost() && $thread->discussion_state == 'moderated')
            {
                //Build payload to send to Slack
                $payload = array (
                    'attachments' =>
                        array (
                        ),
                    'channel' => $channel,
                    'icon_emoji' => $icon,
                    'icon_url' => NULL,
                    'text' =>  $thread->username . $reportPhraseThread . "\n" . $this->app->router('public')->buildLink('canonical:threads', $thread) ,
                    'username' => $app->options()->boostnSlackNotifyuserName,
                );

                $slackService->postToSlack($payload);
                return;
            }

            if($post->message_state == 'moderated')
            {
                //Build payload to send to Slack
                $payload = array (
                    'attachments' =>
                        array (
                        ),
                    'channel' => $channel,
                    'icon_emoji' => $icon,
                    'icon_url' => NULL,
                    'text' => $post->username . $reportPhrasePost . "\n" . $this->app->router('public')->buildLink('canonical:posts', $post) ,
                    'username' => $app->options()->boostnSlackNotifyuserName,
                );

                $slackService->postToSlack($payload);
            }

        }
    }
}
