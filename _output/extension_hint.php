<?php

// ################## THIS IS A GENERATED FILE ##################
// DO NOT EDIT DIRECTLY. EDIT THE CLASS EXTENSIONS IN THE CONTROL PANEL.

namespace BoostN\SlackNotify\XF\Service\Post
{
	class XFCP_Preparer extends \XF\Service\Post\Preparer {}
}

namespace BoostN\SlackNotify\XF\Service\Report
{
	class XFCP_Creator extends \XF\Service\Report\Creator {}
}

namespace BoostN\SlackNotify\XF\Service\User
{
	class XFCP_Registration extends \XF\Service\User\Registration {}
}