<?php

namespace BoostN\SlackNotify\Repository;

use XF\Mvc\Entity\Finder;
use XF\Mvc\Entity\Repository;

class ErrorLog extends Repository
{
    public function getErrors()
    {
        $errors = $this->finder('XF:ErrorLog')
            ->where('boostn_slack_notify', '!=', 1)
            ->order('exception_date', 'DESC')
            ->fetchOne();

        if($errors){
            return $errors->error_id;
        }

        else return 0;
    }

    public function setError($id)
    {
        $error = \XF::em()->find('XF:ErrorLog', $id);

        if($error){
            $error->fastUpdate('boostn_slack_notify', 1);
        }
    }
}
