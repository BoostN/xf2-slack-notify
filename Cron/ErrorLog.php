<?php

namespace BoostN\SlackNotify\Cron;

use XF\Db\Exception;

class ErrorLog
{
    public static function run()
    {
        \XF::app()->jobManager()->enqueueUnique('boostn_slack_errorlog', 'BoostN\SlackNotify:ErrorLog', [], false);
    }
}