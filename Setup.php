<?php

namespace BoostN\SlackNotify;

use XF\AddOn\AbstractSetup;
use XF\AddOn\StepRunnerInstallTrait;
use XF\AddOn\StepRunnerUninstallTrait;
use XF\AddOn\StepRunnerUpgradeTrait;
use XF\Db\Schema\Alter;

class Setup extends AbstractSetup
{
    use StepRunnerInstallTrait;
    use StepRunnerUpgradeTrait;
    use StepRunnerUninstallTrait;

    public function installStep1()
    {
        $this->schemaManager()->alterTable('xf_error_log', function(Alter $table)
        {
            $table->addColumn('boostn_slack_notify', 'boolean')->setDefault(0);
        });
    }

    public function uninstallStep1()
    {
        $this->schemaManager()->alterTable('xf_error_log', function(Alter $table)
        {
            $table->dropColumns('boostn_slack_notify');
        });
    }
}