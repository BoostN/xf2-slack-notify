<?php

namespace BoostN\SlackNotify\Service;

use XF\Service\AbstractService;

class SlackNotify extends AbstractService
{
    public function postToSlack(array $payload)
    {
        $app = \XF::app();
        $epayload = json_encode($payload);

        $webhook = $app->options()->boostnSlackNotifywebHook;


        if(\XF::options()->currentVersionId >= '2010031')
        {
            try
            {
                $client = \XF::app()->http()->client();
                $req = $client->post($webhook,[
                    'headers'=>['Content-Type'=>'application/x-www-form-urlencoded'],
                    'timeout'=> '5',
                    'connect_timeout' => '5',
                    'json'=> $payload
                ]);

                return $req->getBody();
            }
            catch(\GuzzleHttp\Exception\RequestException $e)
            {
                throw new \LogicException("API Call failed! Are your options correct?" . $e);
            }

        }

        else
        {
            try
            {
                $client = \XF::app()->http()->client();
                $req = $client->post($webhook,[
                    'headers'=>['Content-Type'=>'application/x-www-form-urlencoded'],
                    'timeout'=> '2',
                    'connect_timeout' => '2',
                    'body'=> $epayload
                ]);

                return $req->getStatusCode();
            }
            catch(\GuzzleHttp\Exception\RequestException $e)
            {
                throw new \LogicException("API Call failed! Are your options correct?" . $e);
            }
        }
    }
}